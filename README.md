Простой клиент для проверки тестового задания(https://gitlab.com/akombarov/test-for-riga-server)

запуск клиента:

go run main.go

примеры для запросов на запись:

{"action":"put","key":"test1", "value":"value1"}

{"action":"put","key":"test2", "value":"value2"}

{"action":"put","key":"test3", "value":"value3"}


примеры для запросов на чтение:

{"action":"read","key":"test1"}

{"action":"read","key":"test2"}

{"action":"read","key":"test3"}

 
примеры для запросов на удаление:

{"action":"delete","key":"test1"}
 
{"action":"delete","key":"test2"}
 
{"action":"delete","key":"test3"}

пример для запроса на сохранения на диск:

{"action":"save"}


пример для запроса выгрузки данных с диска:

{"action":"load"}
