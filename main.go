package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

func main() {
	conn, _ := net.Dial("tcp", "127.0.0.1:8080")
	reader := bufio.NewReader(os.Stdin)
	for {

		fmt.Print("Text to send: ")

		text, err := reader.ReadString('\n')
		if err != nil {
			log.Println("error1:", err)
		}
		t := time.Now().Add(time.Minute * 10) // устанавливаем время жизни

		text = strings.Replace(text, `}`, `,"ttl":`+fmt.Sprint(t.Unix())+`}`, -1)

		fmt.Fprintf(conn, text)
		log.Println("send:", text)

		message, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			log.Println("error2:", err)
		}
		fmt.Print("Message from server: " + message)
	}
}
